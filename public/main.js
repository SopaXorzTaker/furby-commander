// Some Furby commands.
var COMMANDS = {
    "eventSneeze": 717,
    "eventYawn": 718,

    "commandAttention": 820,

    "actionSleep": 862,
    "actionLaugh": 863,
    "actionBurp": 864,
    "actionFart": 865,
    "actionPurr": 866,
    "actionSneeze": 867,
    "actionSing": 868,
    "actionTalk": 869,
}

// Food commands (arranged by their categories).
var FOOD_COMMANDS = {
    "like": {
        "crunch": 350,
        "chomp": 352,
        "nibbles": 353,
        "slurp": 354,
        "glug": 355
    },

    "dislike": {
        "crunch": 356,
        "chomp": 358,
        "nibbles": 359,
        "slurp": 360,
        "glug": 361
    },

    "sweet": {
        "crunch": 368,
        "chomp": 370,
        "nibbles": 371,
        "slurp": 372,
        "glug": 373
    },

    "sour": {
        "crunch": 374,
        "chomp": 376,
        "nibbles": 377,
        "slurp": 378,
        "glug": 379
    },

    "hot": {
        "crunch": 380,
        "chomp": 382,
        "nibbles": 383,
        "slurp": 384,
        "glug": 385
    },

    "cold": {
        "crunch": 386,
        "chomp": 388,
        "nibbles": 389,
        "slurp": 390,
        "glug": 391
    },

    "spicy": {
        "crunch": 392,
        "chomp": 394,
        "nibbles": 395,
        "slurp": 396,
        "glug": 397
    },

    "refreshing": {
        "crunch": 398,
        "chomp": 400,
        "nibbles": 401,
        "slurp": 402,
        "glug": 403
    },

    "caffeinated": {
        "crunch": 410,
        "chomp": 412,
        "nibbles": 413,
        "slurp": 414,
        "glug": 415
    },

    "spittingOut": {
        "crunch": 416,
        "chomp": 417,
        "nibbles": 418,
        "slurp": 419,
        "glug": 420,
    },

    "throwingUp": {
        "crunch": 421,
        "chomp": 422,
        "nibbles": 423,
        "slurp": 424,
        "glug": 425
    }
}

// The select control
var commandSelect = null;

// The transmit button
var transmitButton = null;

// The output for the command number
var commandOutput = null;

// The food reaction select control
var foodReactionSelect = null;

// The food type select control
var foodTypeSelect = null;

// The custom command code input
var customCommandInput = null;

// Lock the transmit button while transmitting the command.
function lockButton() {
    transmitButton.disabled = true;
}

// Unlocks the ability to transmit.
function unlockButton() {
    transmitButton.disabled = false;
}

function transmit() {
    // Get the command number by its name in the select.
    var command;

    if (commandSelect.value != "random" && commandSelect.value != "custom" && commandSelect.value != "food") {
        // The command isn't one of the special names, so it's in the list.
        command = COMMANDS[commandSelect.value];
    } else {
        if (commandSelect.value == "random") {
            // Random number in range 0-1023.
            command = Math.round(Math.random() * 1023);
        } else if (commandSelect.value == "custom") {
            command = parseInt(customCommandInput.value);
            if (isNaN(command) || command < 0 || command > 1023) {
                alert("Invalid command code: " + command + ", should be 0-1023");
                return;
            }
        } else if (commandSelect.value == "food") {
            // Food command (from the food command list).
            command = FOOD_COMMANDS[foodReactionSelect.value][foodTypeSelect.value];
        }
    }

    // Write the output value
    commandOutput.value = command;

    // Lock the transmit button to avoid multiple commands being played at once.
    lockButton();

    // Transmit
    transmitFurbyCommand(command, unlockButton);
}

// Select the food command when the food type is changed.
function selectFoodCommand() {
    commandSelect.value = "food";
}

function onload() {
    furbyInit();
    commandSelect = document.getElementById("commandSelect");
    transmitButton = document.getElementById("transmitButton");
    commandOutput = document.getElementById("commandOutput");
    foodReactionSelect = document.getElementById("foodReactionSelect");
    foodTypeSelect = document.getElementById("foodTypeSelect");
    customCommandInput = document.getElementById("customCommandInput");
}
