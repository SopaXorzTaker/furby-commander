// This is a simplified port of https://github.com/iafan/Hacksby.

// Checksums for every of 64 possible packets.
var PACKET_CHECKSUMS = [
    [0, 0, 0, 0],
    [0, 1, 1, 0],
    [0, 2, 1, 0],
    [0, 3, 0, 0],
    [1, 0, 2, 3],
    [1, 1, 3, 3],
    [1, 2, 3, 3],
    [1, 3, 2, 3],
    [0, 1, 2, 0],
    [0, 2, 0, 1],
    [0, 3, 3, 0],
    [1, 0, 2, 1],
    [1, 1, 0, 3],
    [1, 2, 2, 2],
    [1, 3, 1, 3],
    [2, 0, 2, 1],
    [0, 2, 2, 0],
    [0, 3, 3, 0],
    [1, 0, 0, 0],
    [1, 1, 1, 0],
    [1, 2, 0, 3],
    [1, 3, 1, 3],
    [2, 0, 0, 0],
    [2, 1, 1, 0],
    [0, 3, 0, 0],
    [1, 0, 1, 1],
    [1, 1, 2, 0],
    [1, 2, 0, 1],
    [1, 3, 2, 3],
    [2, 0, 1, 1],
    [2, 1, 2, 0],
    [2, 2, 0, 1],
    [1, 0, 3, 3],
    [1, 1, 2, 3],
    [1, 2, 2, 3],
    [1, 3, 3, 3],
    [2, 0, 3, 3],
    [2, 1, 2, 3],
    [2, 2, 2, 3],
    [2, 3, 3, 3],
    [1, 1, 1, 3],
    [1, 2, 3, 2],
    [1, 3, 0, 3],
    [2, 0, 3, 1],
    [2, 1, 1, 3],
    [2, 2, 3, 2],
    [2, 3, 0, 3],
    [3, 0, 1, 2],
    [1, 2, 1, 3],
    [1, 3, 0, 3],
    [2, 0, 1, 0],
    [2, 1, 0, 0],
    [2, 2, 1, 3],
    [2, 3, 0, 3],
    [3, 0, 3, 3],
    [3, 1, 2, 3],
    [1, 3, 3, 3],
    [2, 0, 0, 1],
    [2, 1, 3, 0],
    [2, 2, 1, 1],
    [2, 3, 3, 3],
    [3, 0, 2, 2],
    [3, 1, 1, 3],
    [3, 2, 3, 2]
];


// Tone frequencies (0-3 + sync).
var TONE_FREQUENCIES = [
    16376.0, 16938.0, 18624.0, 18062.0, 17500.0
];

// Sync tone index.
var TONE_SYNC = 4;

// Every symbol lasts 20 milliseconds.
var SYMBOL_DURATION = 20e-3;

// The inter-packet gap is 0.5 seconds.
var PACKET_GAP = 0.5;

// The lead gap is one symbol duration.
var LEAD_GAP = SYMBOL_DURATION;

// Our audio context.
var audioCtx = null;


// Converts x to an array of 6 bits.
function unpackBits(x) {
    var n = 32;
    var bits = [];

    // While the mask is non-zero
    while (n) {
        // Is the given bit set?
        bits.push(x & n ? 1 : 0);

        // Shift the mask
        n >>= 1;
    }

    return bits;
}

// Converts the bit array x to base-4 symbols.
function bitsToSymbols(x) {
    var symbols = [];

    // Iterate over groups of two bits
    for (var i = 0; i < x.length; i += 2) {
        // Pack two bits into one digit
        symbols.push(x[i]*2 + x[i+1]);
    }

    return symbols;
}

// Converts a command number (0 - 1023) to the symbols of a packet.
function makePacket(command) {
    // The first packet is just the five higher bits of the command.
    var packet1 = command >> 5;

    // The second packet is the lower bits OR'ed with 32.
    var packet2 = (command & 0x1f) | 0x20;

    // Convert both packets to sequences of symbols.
    var symbols1 = bitsToSymbols([1, 1].concat(unpackBits(packet1))).concat(PACKET_CHECKSUMS[packet1]).concat([1, 0, 3, 2]);
    var symbols2 = bitsToSymbols([1, 1].concat(unpackBits(packet2))).concat(PACKET_CHECKSUMS[packet2]).concat([1, 0, 3, 2]);

    return [symbols1, symbols2];
}

// Generates zero samples for a given duration.
function silence(duration, sampleRate) {
    var samples = [];

    // Find the duration in samples.
    var numSamples = Math.round(duration * sampleRate);

    // Append numSamples zeros.
    for (var i = 0; i < numSamples; i++)
        samples.push(0.0)

    return samples;
}

// The sinc function (for pulse shaping).
function sinc(x) {
    if (x != 0.0)
        return Math.sin(x) / x;

    return 1.0;
}

// Generates a tone.
function tone(index, sampleRate) {
    var samples = [];

    // The frequency of the tone.
    var frequency = TONE_FREQUENCIES[index];

    // The duration of the tone (in samples).
    var duration = Math.round(sampleRate * SYMBOL_DURATION);

    // Create a sine wave.
    for (var i = 0; i < duration; i++) {
        var sine_phase = frequency * i * Math.PI * 2.0 / sampleRate;
        var sinc_phase = (i / duration - 0.5) * Math.PI * 2.0;

        samples.push(Math.sin(sine_phase) * sinc(sinc_phase));
    }

    return samples;
}

// Generates the samples of a packet.
function packet(symbols, sampleRate) {
    // Start with a sync
    var samples = tone(TONE_SYNC, sampleRate);

    // Add the symbols separated by the sync tone.
    for (var i = 0; i < symbols.length; i++) {
        // Symbol
        samples = samples.concat(tone(symbols[i], sampleRate));

        // Sync
        samples = samples.concat(tone(TONE_SYNC, sampleRate));
    }

    return samples;
}

// Transmits a command.
function transmitFurbyCommand(command, onended) {
    // Make a packet of the command.
    var symbols = makePacket(command);

    // We have two sub-packets.
    var packet1 = symbols[0];
    var packet2 = symbols[1];

    // The sample rate we're going to use.
    var sampleRate = audioCtx.sampleRate;

    // Generate the lead gap
    var samples = silence(LEAD_GAP, sampleRate);

    // Append the first packet.
    samples = samples.concat(packet(packet1, sampleRate));

    // Append the inter-packet gap.
    samples = samples.concat(silence(PACKET_GAP, sampleRate));

    // Append the second packet.
    samples = samples.concat(packet(packet2, sampleRate));

    // Append another gap at the end.
    samples = samples.concat(silence(LEAD_GAP, sampleRate));

    // Create a buffer for the samples and copy them over.
    var audioBuffer = audioCtx.createBuffer(2, samples.length, audioCtx.sampleRate);

    // Copy the samples to both stereo channels.
    var typedSamples = new Float32Array(samples);
    audioBuffer.copyToChannel(typedSamples, 0);
    audioBuffer.copyToChannel(typedSamples, 1);

    // Create a buffer source.
    var source = audioCtx.createBufferSource();

    // Use our buffer.
    source.buffer = audioBuffer;

    // The end callback.
    source.onended = onended;

    // Connect to our audio context
    source.connect(audioCtx.destination);

    // Play the samples.
    source.start();
}


// Initialize
function furbyInit() {
    audioCtx = new AudioContext();
    console.log("created AudioContext", audioCtx);
}
